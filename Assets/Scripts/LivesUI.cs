﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivesUI : MonoBehaviour
{
    public Text Lives;

    // Update is called once per frame
    void Update()
    {
        Lives.text = string.Format("{0} LIVES", PlayerStats.Lives);

    }
}
